import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import socketIO from "socket.io-client";
import Home from "./components/Home";
import Products from "./components/Products";
import AddProduct from "./components/AddProduct";
import BidPoduct from "./components/BidProduct";
import Nav from "./components/Nav";
import { useEffect, useState } from "react";
const socket = (socketIO as any).connect("http://127.0.0.1:4000");
export interface Product {
  name: string;
  coverImage: string;
  price: number;
  last_bidder?: string;
  owner: string;
}
function App() {
  const [products, setProducts] = useState<Product[] | null>(null);
  const [loading, setLoading] = useState(true);
  const [countdown, setCountdown] = useState(20);

  useEffect(() => {
    socket.on("updatedCountdown", (initialCountdown: number) => {
      setCountdown(initialCountdown);
    });
    socket.on("updateCountdown", (updatedCountdown: number) => {
      setCountdown(updatedCountdown);
    });

    return () => {
      socket.disconnect();
    };
  }, [socket]);
  const fetchProducts = () => {
    fetch("http://127.0.0.1:4000/api")
      .then((res) => res.json())
      .then((data) => {
        setProducts(data.products);
        setLoading(false);
      });
  };
  const fetchCountdown = () => {
    fetch("http://127.0.0.1:4000/api/countdown")
      .then((res) => res.json())
      .then((data: any) => {
        setCountdown(data.countdown);
      });
  };
  /*   useEffect(() => {
    const countdownInterval = setInterval(() => {
      setCountdown((prevCountdown) =>
        prevCountdown > 0 ? prevCountdown - 1 : 0
      );
    }, 1000);
    return () => {
      clearInterval(countdownInterval);
    };
  }, []); */

  useEffect(() => {
    socket.on("bidProductResponse", () => {
      fetchProducts();
      fetchCountdown();
    });
    fetchProducts();
    fetchCountdown();
  }, [socket]);
  console.log(countdown);
  return (
    <Router>
      <div className="min-h-screen bg-slate-300">
        <Nav socket={socket} />
        <div> {countdown} </div>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route
            path="/products"
            element={
              <Products socket={socket} products={products} loading={loading} />
            }
          />
          <Route
            path="/products/add"
            element={<AddProduct socket={socket} />}
          />
          <Route
            path="/products/bid/:name"
            element={
              <BidPoduct
                socket={socket}
                products={products}
                countdown={countdown}
                setCountdown={setCountdown}
              />
            }
          />
        </Routes>
      </div>
    </Router>
  );
}
export default App;
