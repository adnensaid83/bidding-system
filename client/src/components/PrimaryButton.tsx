import { ReactNode } from "react";
import { NavLink } from "react-router-dom";

const PrimaryButton = ({
  path,
  children,
}: {
  path: string;
  children: ReactNode;
}) => {
  return (
    <NavLink
      to={path}
      className="bg-orange text-white px-16 py-2 rounded-md text-base border-orange border-2 transition duration-300 md:text-l md:px-20 md:hover:bg-transparent md:hover:text-orange"
    >
      {children}
    </NavLink>
  );
};

export default PrimaryButton;
