import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function AddProduct({ socket }: { socket: any }) {
  const [name, setName] = useState("");
  const [price, setPrice] = useState<number>(0);
  const navigate = useNavigate();
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    socket.emit("addProduct", {
      name,
      price,
      owner: localStorage.getItem("username"),
    });
    navigate("/products");
  };
  const handlePriceChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newPrice = parseFloat(e.target.value);
    setPrice(isNaN(newPrice) ? 0 : newPrice);
  };
  return (
    <div>
      <div className="addproduct__container">
        <h1>Add Product</h1>
        <form className="addProduct__form" onSubmit={handleSubmit}>
          <label htmlFor="name">Nam of the product</label>
          <input
            type="text"
            name="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
          <label htmlFor="price">Starting price</label>
          <input
            type="text"
            name="price"
            value={price}
            onChange={handlePriceChange}
            required
          />
          <button className="bidProduct__cta">SEND</button>
        </form>
      </div>
    </div>
  );
}
