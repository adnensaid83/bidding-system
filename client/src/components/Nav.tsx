import { useEffect, useState } from "react";
import Navbar from "./Navbar";
export default function Nav({ socket }: { socket: any }) {
  const [notification, setNotification] = useState("");

  //Listen after a product is added
  useEffect(() => {
    socket.on("addProductResponse", (data: any) => {
      setNotification(
        `@${data.owner} just added ${data.name} worth $${Number(
          data.price
        ).toLocaleString()}`
      );
    });
  }, [socket]);

  //Listens after a user places a bid
  useEffect(() => {
    socket.on("bidProductResponse", (data: any) => {
      setNotification(
        `@${data.last_bidder} just bid ${data.name} for $${Number(
          data.userInput
        ).toLocaleString()}`
      );
    });
  }, [socket]);

  const social = [{ id: "1", title: "Instagram", url: "" }];
  const sections = [{ name: "Home", slug: "" }];
  return (
    <Navbar
      theme="light"
      handleTheme={() => {}}
      logo=""
      pages={sections ? sections : []}
      address={"Lyon, France"}
      socials={social ? social : []}
      phone={"07 81 15 29 46"}
    />
  );
}

{
  /* <nav className="bg-white">
      <div className="text-xl ">
        <h2 style={{ color: "black" }}>Bid Items</h2>
      </div>
      <div>
        <p style={{ color: "red" }}> {notification} </p>
      </div>
    </nav> */
}
