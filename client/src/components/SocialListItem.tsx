import { NavLink } from "react-router-dom";
import { Behance, Instagram, Linkedin, Gitlab } from "../assets";
import { SocialI } from "../interfaces/Social.interface";

const SocialListItem = ({ social }: { social: SocialI }) => {
  const { title, url } = social;

  return (
    <li
      className={`w-[36px] h-[36px] flex items-center justify-center p-1 md:hover:text-white md:hover:bg-dark md:hover:border-dark`}
    >
      <NavLink to={url}>
        {title === "Linkedin" ? (
          <Linkedin />
        ) : title === "Instagram" ? (
          <Instagram />
        ) : title === "Behance" ? (
          <Behance />
        ) : title === "Gitlab" ? (
          <Gitlab />
        ) : null}
      </NavLink>
    </li>
  );
};

export default SocialListItem;
