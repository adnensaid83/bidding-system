import { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { Product } from "../App";

export default function BidPoduct({
  socket,
  products,
  countdown,
  setCountdown,
}: {
  socket: any;
  products: Product[] | null;
  countdown: number;
  setCountdown: (n: number) => void;
}) {
  const [userInput, setUserInput] = useState<number>(0);
  const { name } = useParams();
  const product: any = products?.filter((p) => p.name === name)[0];
  const { coverImage, price, last_bidder } = product;
  const navigate = useNavigate();
  const [error, setError] = useState(false);

  const handleAutomaticBid = () => {
    // Logique pour déterminer le dernier gagnant
    const lastBidWinner = localStorage.getItem("username");
    socket.emit("bidProduct", {
      userInput,
      last_bidder: lastBidWinner,
      name,
    });
    if (countdown <= 20) {
      // Réinitialiser le compte à rebours à 10 secondes si un clic se produit entre 10 et 0 secondes
      setCountdown(20);
    }
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (userInput > Number(price)) {
      socket.emit("bidProduct", {
        userInput,
        last_bidder: localStorage.getItem("username"),
        name,
      });
      if (countdown <= 20) {
        handleAutomaticBid();
      }
    } else {
      setError(true);
    }
  };
  const handleUserInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newInput = parseFloat(e.target.value);
    setUserInput(isNaN(newInput) ? 0 : newInput);
  };
  return (
    <div className="container mx-auto px-6">
      <div className="flex flex-col items-center">
        <img src={coverImage} alt={name} width={"500px"} />
        <form className="bidProduct__form" onSubmit={handleSubmit}>
          <h2 className="text-center font-semibold text-2xl mb-6"> {name} </h2>
          <div>
            Enchère en cours : <strong>{price} €</strong>
          </div>
          <div>
            dernier_enchérisseur:
            <strong>{last_bidder || "None"}</strong>
          </div>
          <label htmlFor="amount">Bidding Amount</label>
          {error && (
            <p style={{ color: "red" }}>
              The bidding amount must be greater than {price}
            </p>
          )}
          <input
            type="number"
            name="amount"
            value={userInput}
            onChange={handleUserInputChange}
            required
            disabled={countdown <= 0}
          />
        </form>
        <div> {countdown} </div>
      </div>
    </div>
  );
}
