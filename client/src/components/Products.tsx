import { useNavigate } from "react-router-dom";
import PrimaryButton from "./PrimaryButton";
import { Product } from "../App";

export default function Products({
  socket,
  products,
  loading,
}: {
  socket: any;
  products: Product[] | null;
  loading: boolean;
}) {
  const navigate = useNavigate();

  const handleBidBtn = (product: Product) =>
    navigate(`/products/bid/${product.name}`);

  return (
    <div className="container mx-auto px-6">
      <div className="flex flex-col gap-12">
        <div className="text-end">
          <PrimaryButton path="/products/add">Ajouter un produit</PrimaryButton>
        </div>
        <div className="grid grid-cols-4 gap-4">
          {loading ? (
            <div>
              <h2>Loading...</h2>
            </div>
          ) : (
            products &&
            products.map((product: Product) => (
              <div
                key={`${product.name}${product.price}`}
                className="text-base bg-green flex flex-col items-center gap-2 p-4"
              >
                <div className="flex-1">
                  <img src={product.coverImage} alt={product.name} />
                </div>
                <div className="text-center font-semibold">{product.name}</div>
                <div>
                  Enchère en cours : <strong>{product.price} €</strong>
                </div>
                <div> {product.last_bidder || "None"} </div>
                <div>
                  Vendeur: <strong>{product.owner}</strong>
                </div>
                <div>
                  <button
                    className="bg-green200 text-white px-16 py-2 rounded-md text-base border-green200 border-2 transition duration-300 md:text-l md:px-20 md:hover:bg-transparent md:hover:text-green200"
                    onClick={() => handleBidBtn(product)}
                  >
                    Enchérir
                  </button>
                </div>
              </div>
            ))
          )}
        </div>
      </div>
    </div>
  );
}
