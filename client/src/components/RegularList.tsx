import { ReactElement } from "react";

export type MyGenericType<TData> = {
  data: TData;
};

type RegularListProps<T> = {
  items: T[];
  resourceName: string;
  itemComponent: any;
};

export function RegularList<T>({
  items,
  resourceName,
  itemComponent: ItemComponent,
}: RegularListProps<T>): ReactElement {
  return (
    <>
      {items.map((item, index) => (
        <ItemComponent
          key={index}
          {...{ [resourceName]: item, isEven: index % 2 !== 0, index: index }}
        />
      ))}
    </>
  );
}
