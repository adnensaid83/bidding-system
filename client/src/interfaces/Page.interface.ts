export interface PageI {
  id?: string;
  name: string;
  slug: string;
  order?: number;
}
