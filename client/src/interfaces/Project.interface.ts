import { ImageI } from "./Image.interface";

export interface CoverImageI {
  responsiveImage: {
    srcSet: string;
    webpSrcSet: string;
    sizes: string;
    src: string;
    width: number;
    height: number;
    aspectRatio: number;
    alt: string;
    title: string;
    base64: string;
  };
}
export interface ProjectI {
  id: string;
  title: string;
  description: string;
  image: ImageI[];
  thumbnail: ImageI;
  coverImage: CoverImageI;
  maquette: ImageI;
  tech: ImageI[];
  web: string;
  git: string;
}
