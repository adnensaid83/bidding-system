export interface ImageI {
  alt: string;
  url: string;
}
