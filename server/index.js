const express = require("express");
const app = express();
const PORT = 4000;
const http = require("http").Server(app);
const cors = require("cors");
const fs = require("fs");
const socketIO = require("socket.io")(http, {
  cors: {
    origin: "http://127.0.0.1:5173",
  },
});
// Gets the JSON file and parse the file into JavaScript object
const rawData = fs.readFileSync("data.json");
const productData = JSON.parse(rawData);
let countdown = 40;
const countdownInterval = 1000;

app.use(cors());

function findProduct(nameKey, productsArray, last_bidder, new_price) {
  for (let index = 0; index < productsArray.length; index++) {
    if (productsArray[index].name === nameKey) {
      productsArray[index].last_bidder = last_bidder;
      productsArray[index].price = new_price;
    }
  }
  const stringData = JSON.stringify(productData, null, 2);
  fs.writeFile("data.json", stringData, (err) => {
    console.error(err);
  });
}

function getProductByName(nameKey, productsArray) {
  return productsArray.find((product) => product.name === nameKey);
}

socketIO.on("connection", (socket) => {
  console.log(`⚡: ${socket.id} user just connected!`);

  // Broadcast countdown updates on connection

  socket.emit("updatedCountdown", countdown);

  const countdownIntervalId = setInterval(() => {
    countdown--;
    if (countdown <= 0) {
      clearInterval(countdownIntervalId);
      // Handle countdown reaching zero, for example, end of auction logic
      console.log("Countdown reached zero. Auction ended.");
    } else {
      // Broadcast the updated countdown to all clients
      socketIO.emit("updateCountdown", countdown);
    }
  }, countdownInterval);

  socket.on("disconnect", () => {
    console.log("🔥: A user disconnected");
    clearInterval(countdownIntervalId);
  });

  // listen to the addProduct event
  socket.on("addProduct", (data) => {
    productData["products"].push(data);
    const stringData = JSON.stringify(productData, null, 2);
    fs.writeFile("data.json", stringData, (err) => {
      console.log(err);
    });
    //Sends back the data after adding a new product
    socket.broadcast.emit("addProductResponse", data);
  });

  //Listens for new bids from the client
  socket.on("bidProduct", (data) => {
    findProduct(
      data.name,
      productData["products"],
      data.last_bidder,
      data.userInput
    );
    // Broadcast the updated countdown to all clients
    socketIO.emit("updateCountdown", countdown);
    //Sends back the data after placing a bid
    socket.broadcast.emit("bidProductResponse", { data, countdown });
  });
});

app.get("/api/countdown", (req, res) => {
  res.json({ countdown });
});

app.get("/api", (req, res) => {
  res.json(productData);
});

app.get("/api/product/:name", (req, res) => {
  const { name } = req.params;
  const product = getProductByName(name, productData.products);

  if (product) {
    res.json(product);
  } else {
    res.status(404).json({ error: "Product not found" });
  }
});

http.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
