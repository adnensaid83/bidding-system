# Real-time Auction System

![auction](client/src/assets/Auction.png)

# Description

Like an actual auction, if you bid for a product, you get counterbids from other bidders. The auction runs on the “fast” decision bid, where somebody else will win or outbid you if you don’t bid fast enough.
in the last 10 seconds if you have a bet on the product the countdown counter returns to 10

# Languages utilisés

- REACTJS, react-router, Socket.io, express, cors, nodemon
- TAILWINDCSS

# Comment ça marche

1. **Installation des dépendances:** Run the `npm install` command to install all necessary dependencies.
2. **Run the server:** Run the server with Nodemon by using the command `cd server` `npm start`
3. **Start the ReactJS server:** Start the ReactJS server by using the command `cd client` `npm run dev`
